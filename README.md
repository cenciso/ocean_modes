# Normal Vertical Modes of the Ocean

The aim of this project is to solve the Sturm–Liouville problem for the vertical
modes decomposition of a linear stratified ocean to describe simple dynamics.
The boundary conditions are of a free-surface ocean with a constant depth [1].

The input data was obtainded from GODAS (potential temperature and salinity)
for the 1980-2018 period.

This is still a work in progress.

Bibliography

[1] A. J. Clarke, An introduction to the dynamics of El Niño & the southern 
oscillation, 1. ed. Amsterdam: Academic Press, 2008.
